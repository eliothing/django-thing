![](https://elioway.gitlab.io/eliothing/django-thing/elio-django-Thing-logo.png)

> Django, **the elioWay**

# django-thing

![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Django models, views, forms, and templates, built out of schema.org "Things", **the elioWay**.

- [dna Documentation](https://elioway.gitlab.io/eliothing/django-thing)

## Installing

```shell
pip install elio-dna
```

- [Installing dna](https://elioway.gitlab.io/eliothing/django-thing/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/eliothing/django-thing.git
cd dna
virtualenv --python=python3 venv-dna_django
source venv-dna_django/bin/activate
pip install -r requirements/local.txt
./init_chromosomes.sh
```

## Testing

Because the core feature of **django-thing** is building **Django** Models dynamically, some tests need to be run isolated.

## Nutshell

```
django-admin runserver
black .
```

- [dna Quickstart](https://elioway.gitlab.io/eliothing/django-thing/quickstart.html)
- [dna Credits](https://elioway.gitlab.io/eliothing/django-thing/credits.html)

![](https://elioway.gitlab.io/eliothing/django-thing/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
