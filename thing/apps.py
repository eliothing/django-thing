# -*- encoding: utf-8 -*-
from django.apps import AppConfig


class ThingDjangoConfig(AppConfig):
    name = "thing"
    verbose_name = "elioWay Django ThingDjango"
