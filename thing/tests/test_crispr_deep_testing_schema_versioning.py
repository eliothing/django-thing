"""
- Alerts to changes in new versions of schema.org.
- Backwards compatibility."""
# -*- encoding: utf-8 -*-
import os
import pytest
from django.test import SimpleTestCase, tag
from thing.crispr import Crispr


SCHEMA_PATH = "schemaorg/data/releases/3.9/all-layers.jsonld"


@tag("thing", "slow")
class TestReplicateDjangoModelFields(SimpleTestCase):
    def test_crispr_schema_versioning_members(self):
        """Correct as of 3.9."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        self.assertEqual(809, len(crispr.classes))
        self.assertEqual(1268, len(crispr.properties))
        self.assertEqual(263, len(crispr.enumerated))
        self.assertEqual(11, len(crispr.primitives))

        print([p['rdfs:label'] for p in crispr.enumerated])
        assert False
        self.assertEqual(
            set(
                [
                    "http://schema.org/Boolean",
                    "http://schema.org/DateTime",
                    "http://schema.org/Date",
                    "http://schema.org/Number",
                    "http://schema.org/Text",
                    "http://schema.org/Time",
                ]
                + [
                    "http://schema.org/Duration",
                    "http://schema.org/Float",
                    "http://schema.org/Integer",
                    "http://schema.org/Quantity",
                    "http://schema.org/URL",
                ]
            ),
            set(crispr.primitives),
        )

    def test_crispr_schema_versioning_default_Thing(self):
        """Highest dependency otherwise only primitive types because no depth."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        class_dependencies_of = crispr.class_dependencies_of(
            ["http://schema.org/Thing"]
        )
        self.assertEqual(1, len(class_dependencies_of))
        self.assertEqual(
            set(["http://schema.org/Thing"]), set(class_dependencies_of)
        )

    def test_crispr_schema_versioning_default_MusicComposition(self):
        """Three tier subclasses otherwise only primitive types because no depth."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        class_dependencies_of = crispr.class_dependencies_of(
            ["http://schema.org/MusicComposition"]
        )
        self.assertEqual(4, len(class_dependencies_of))
        self.assertEqual(
            set(
                [
                    "http://schema.org/CreativeWork",
                    "http://schema.org/Intangible",
                    "http://schema.org/MusicComposition",
                    "http://schema.org/Thing",
                ]
            ),
            set(class_dependencies_of),
        )

    def test_crispr_schema_versioning_default_Notary(self):
        """Four tier multi subclasses (LocalBusiness=Place+Org) otherwise primitive types."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        class_dependencies_of = crispr.class_dependencies_of(
            ["http://schema.org/Notary"]
        )
        self.assertEqual(6, len(class_dependencies_of))
        # Subclass dependency otherwise primitive types.
        self.assertEqual(
            set(
                [
                    "http://schema.org/LegalService",
                    "http://schema.org/LocalBusiness",
                    "http://schema.org/Notary",
                    "http://schema.org/Organization",
                    "http://schema.org/Place",
                    "http://schema.org/Thing",
                ]
            ),
            set(class_dependencies_of),
        )

    def test_crispr_schema_versioning_depth_1_Thing(self):
        """Highest dependency with more primitive types."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        class_dependencies_of = crispr.class_dependencies_of(
            ["http://schema.org/Thing"], max_depth=1
        )
        self.assertEqual(9, len(class_dependencies_of))
        # With some property types
        self.assertEqual(
            set(
                [
                    "http://schema.org/Action",
                    "http://schema.org/CreativeWork",
                    "http://schema.org/Event",
                    "http://schema.org/ImageObject",
                    "http://schema.org/Intangible",
                    "http://schema.org/MediaObject",
                    "http://schema.org/PropertyValue",
                    "http://schema.org/Thing",
                    "http://schema.org/StructuredValue",
                ]
            ),
            set(class_dependencies_of),
        )

    def test_crispr_schema_versioning_depth_2_Thing(self):
        """Highest dependency with more and more primitive types."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        class_dependencies_of = crispr.class_dependencies_of(
            ["http://schema.org/Thing"], max_depth=2
        )
        self.assertEqual(41, len(class_dependencies_of))
        self.assertEqual(
            set(
                [
                    "http://schema.org/Thing",
                    "http://schema.org/ImageObject",
                    "http://schema.org/PropertyValue",
                    "http://schema.org/CreativeWork",
                    "http://schema.org/Event",
                    "http://schema.org/Action",
                    "http://schema.org/MediaObject",
                    "http://schema.org/Distance",
                    "http://schema.org/QuantitativeValue",
                    "http://schema.org/MediaSubscription",
                    "http://schema.org/Place",
                    "http://schema.org/NewsArticle",
                    "http://schema.org/Organization",
                    "http://schema.org/Person",
                    "http://schema.org/Clip",
                    "http://schema.org/AudioObject",
                    "http://schema.org/Audience",
                    "http://schema.org/VideoObject",
                    "http://schema.org/AlignmentObject",
                    "http://schema.org/PublicationEvent",
                    "http://schema.org/Review",
                    "http://schema.org/AggregateRating",
                    "http://schema.org/ItemList",
                    "http://schema.org/Offer",
                    "http://schema.org/Language",
                    "http://schema.org/Product",
                    "http://schema.org/InteractionCounter",
                    "http://schema.org/Rating",
                    "http://schema.org/DefinedTerm",
                    "http://schema.org/Comment",
                    "http://schema.org/CorrectionComment",
                    "http://schema.org/StructuredValue",
                    "http://schema.org/Intangible",
                    "http://schema.org/QualitativeValue",
                    "http://schema.org/Enumeration",
                    "http://schema.org/EventStatusType",
                    "http://schema.org/PostalAddress",
                    "http://schema.org/ActionStatusType",
                    "http://schema.org/EntryPoint",
                    "http://schema.org/Article",
                    "http://schema.org/ContactPoint",
                ]
            ),
            set(class_dependencies_of),
        )

    def test_crispr_schema_versioning_deep_testing_Thing(self):
        """Highest dependency with many, many more primitive types."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        for depth, class_len in {
            3: 94,
            4: 118,
            5: 127,
            6: 128,
            7: 128,  # eventually we reach a max
        }.items():
            class_dependencies_of = crispr.class_dependencies_of(
                ["http://schema.org/Thing"], max_depth=depth
            )
            self.assertEqual(class_len, len(class_dependencies_of))

    def test_crispr_schema_versioning_deep_testing_MusicComposition(self):
        """Three tier subclasses with depth."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        for depth, class_len in {
            1: 32,
            2: 94,
            3: 120,
            4: 128,
            5: 128,  # eventually we reach a max
        }.items():
            class_dependencies_of = crispr.class_dependencies_of(
                ["http://schema.org/MusicComposition"], max_depth=depth
            )
            self.assertEqual(class_len, len(class_dependencies_of))

    def test_crispr_schema_versioning_deep_testing_Notary(self):
        """Four tier multi subclasses (LocalBusiness=Place+Org) with depth."""
        self.assertTrue(os.path.isfile(SCHEMA_PATH))
        crispr = Crispr(SCHEMA_PATH)
        for depth, class_len in {
            1: 43,
            2: 92,
            3: 113,
            4: 122,
            5: 130,
            6: 131,
            7: 131,  # eventually we reach a max
        }.items():
            class_dependencies_of = crispr.class_dependencies_of(
                ["http://schema.org/Notary"], max_depth=depth
            )
            self.assertEqual(class_len, len(class_dependencies_of))
