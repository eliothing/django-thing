# -*- encoding: utf-8 -*-
import factory
from django.apps import apps


def every_factory(model_name, app_label, *args, **kwargs):
    class ThingFactory(factory.django.DjangoModelFactory):
        class Meta:
            model = apps.get_model(model_name=model_name, app_label=app_label)

    return ThingFactory(*args, **kwargs)
