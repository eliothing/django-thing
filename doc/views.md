# django-thing Views

All the Mixins described in [View Mixins](/eliothing/django-thing/view_mixins) are combined with standard Django class based views on the `thing.views` file, which you can import into your own views file.

## `EveryCreateViewMixin`

Create any Schema Type mentioned in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryGetObjectMixin`, `EveryEditMixin`, `CreateView`
- **URL**: `/cr/page_name/thing_name`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: "active|label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'form': <standard django modelform>,
}
```

## `EveryUpdateViewMixin`

Update any Schema Type mentioned in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryGetObjectMixin`, `EveryEditMixin`, `UpdateView`
- **URL**: `/u/page_name/thing_name/pk`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: "active|label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'object': <standard django model object>,
  'form': <standard django modelform>,
}
```

## `EveryDeleteViewMixin`

Delete any Schema Type mentioned in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryGetObjectMixin`, `EveryQuerySetMixin`, `EveryDeleteMixin`, `DeleteView`
- **URL**: `/d/page_name/thing_name/pk`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: "active|label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'object': <standard django model object>,
}
```

## `EveryEngageViewMixin`

View on a single page any Schema Type mentioned in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryQuerySetMixin`, `EveryGetObjectMixin`, `DetailView`
- **URL**: `/e/page_name/thing_name/pk`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'many_thing': "<many_thing>",
  'prop_name': "<prop_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: "active|label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...repeat... } },
  },
  'object': <standard django model object>,
  'many': { "<field1>": <queryset>, "<field2>": <queryset>, ... },
}
```

## `EveryManyViewMixin`

Handles properties of any Schema Type which have been given an `iterable` role `SITE_THING`. _NB: Feature in development_

- **Mixins**: `EveryThingMixin`, `TemplateView`
- **URL**: `/i/page_name/thing_name/pk/field_name`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: "active|label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'object': <standard django model object>,
  'form': <standard django modelform>,
}
```

## `EveryOptimizeViewMixin`

A special kind of view from which you can create reports, graphs, or provide settings and features to administrate any Schema Type. _NB: Feature in development_

- **Mixins**: `EveryThingMixin`, `TemplateView`
- **URL**: `/o/page_name/thing_name`

Work in progress.

## `EveryListViewMixin`

View on a single page a queryset matching a Schema Type mentioned in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryQuerySetMixin`, `ListView`
- **URL**: `/l/page_name/thing_name`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: ""|"active" },
      "<menu1>": { status: "label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'object_list': <standard django queryset>,
  'paginator': <standard django paginator>,
  'page_obj': <standard django paginator page_obj>,
}
```

## `EveryPageViewMixin`

View all the content of Schema Types mentioned in a `page` in your `SITE_THING`.

- **Mixins**: `EveryThingMixin`, `EveryPageMixin`, `TemplateView`
- **URL**: `/p/page_name`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: ""|"active" },
      "<menu1>": { status: "label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  },
  'page_engages': {
    "ModelName1": {
      "thing_name": "<thing_name>",
      "engage": <standard django model object>,
      "elio_key": "<elio_key>",
      "many": { "<field1>": <queryset>, "<field2>": <queryset>, ... }
    }, ...
  },
  'page_lists': {
    "ModelName1": {
      "thing_name": "<thing_name>",
      "list": <standard django model queryset>,
      "elio_key": "<elio_key>",
      "is_paginated": <True|False>,
      "paginator": <standard django paginator>,
      "paginate_type": "<paginator_querystring_key>",
    }, ...
  }
}
```

## `EveryHomeViewMixin`

The entry point of the site which you can use as Splash Screen, a Login Page or other static content for your webapp.

- **Mixins**: `EveryThingMixin, TemplateView`
- **URL**: `/`

**Template Context**

```
{
  'app_name': "<app_name>",
  'home_page': "<home_page>",
  'page_name': "<page_name>",
  'page_slug': "<page_slug>",
  'thing_name': "<thing_name>",
  'page_menu': {
      "<menu1>": { "url": "/url1", status: ""|"active" },
      "<menu1>": { status: "label" },
      "<menu2>": { "url": "/url2", status: "", "page": { ...rpt... } },
  }
}
```

## Your views file

Your `app_name.views.py` file should look something like this, using **django-thing** Views above. You can then build on the available context variables and view properties described above.

Out of the box, the only thing which needs setting is a template file and `template_name` for each **django-thing** view type. These are described in the [templates](/eliothing/django-thing/templates) section of this documentation.

```
# -*- encoding: utf-8 -*-
from thing.views import (
    EveryCreateViewMixin,
    EveryUpdateViewMixin,
    EveryDeleteViewMixin,
    EveryEngageViewMixin,
    EveryListViewMixin,
    EveryManyViewMixin,
    EveryOptimizeViewMixin,
    EveryPageViewMixin,
    EveryHomeViewMixin,
)


class MyAppCreateView(EveryCreateViewMixin):
  template_name = "my_app/thing_form.html"

class MyAppUpdateView(EveryUpdateViewMixin):
  template_name = "my_app/thing_form.html"

class MyAppDeleteView(EveryDeleteViewMixin):
  template_name = "my_app/thing_delete.html"

class MyAppEngageView(EveryEngageViewMixin):
  template_name = "my_app/thing_engaged.html"

class MyAppListView(EveryListViewMixin):
  template_name = "my_app/thing_listed.html"

class MyAppManyView(EveryManyViewMixin):
  template_name = "my_app/thing_listed.html"

class MyAppPageView(EveryPageViewMixin):
  template_name = "my_app/thing_page.html"

class MyAppHomeView(EveryHomeViewMixin):
  template_name = "my_app/home_page.html"
```
