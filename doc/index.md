> Django, **the elioWay**

**django-thing** is an experiment in expediancy. With this library you can rapidly build Django web apps which are especially suitable for collecting data, but will also make perfect blogs, travel guides, journals, and even galleries.

**django-thing** drops the pretense that one thing is so different from another. Instead of inventing new Models, **django-thing** expects you to use any of the hundreds of available Schema Types in microdata sites like <https://schema.org>. Because the application knows what to expect, this enables web developers to almost entirely skip the CRUD stage of work. In almost all cases there is already a Model with all the fields you need in Schema.

The obvious advantage of using Schema to model your databases is being able to automatically render your content with prefect microdata.

To create a website with **django-thing** you don't need to create any models or forms. Simply define the shape of your website as a simple JSON dictionary and let **django-thing** do the rest. All the models, views, forms and templates will work out of the box. You really can launch a basic website or application in less than 10 minutes.

# Full Documentation

- [Installing](/eliothing/django-thing/installing.html)
- [Quickstart](/eliothing/django-thing/quickstart.html)
- [SITE_THING](/eliothing/django-thing/site_thing)
- [Settings](/eliothing/django-thing/settings)
- [Models](/eliothing/django-thing/models)
- [View Mixins](/eliothing/django-thing/view_mixins)
- [Views](/eliothing/django-thing/views)
- [Urls](/eliothing/django-thing/urls)
- [Forms](/eliothing/django-thing/forms)
- [Templates](/eliothing/django-thing/templates)
- [Browse Things](/eliothing/django-thing/browse)
- [genome](/eliothing/django-thing/genome)

# The elioWay

**django-thing** encourages you to work **the elioWay**.

**elio** is an acronym for:

- engage
- list
- iterate
- optimize

There's nothing unique about this pattern... developers write apps like this all the time. Out of the box, **django-thing** is geared toward this approach and the app leads the user to _engage_ with a "Thing", _list_ "Things" related to it, then _iterate_ the process of _engaging_ with "Things" in the _list_.

In this way a rich, semantically connected array of "Things" are built _iteratively_.

The _optimize_ stage is where you - as a developer - will be more likely to add custom features. like reporting on, merging, or cleaning records. I will write more about _optimize_ later.
