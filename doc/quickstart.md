# Quickstart django-thing

- [django-thing Prerequisites](/eliothing/django-thing/prerequisites.html)
- [Installing django-thing](/eliothing/django-thing/installing.html)

## Nutshell

- Call `yo thing`.

- Define your `SITE_THING`.

- Edit your templates.

With this library you can prototype a new website quickly, as long as you do things **the elioWay**.

## Glossary

- _models_ and _Types_ and _things_ and _classes_ are the same thing. On schema.org they are called "classes" or "Types" (and properties can also be data types). All _Types_ on <https://schema.org> are ancestors of the highest class called "Thing". In Django all these are represented as Models.

- _fields_ and _properties_ are the same thing. On <https://schema.org> they are called "properties"; and these are mapped to Django "fields" of the same name.

## Exercise 1: Calling `yo thing`

Start by creating a folder for your app, and typing `yo thing`. We will use "Frankenstein" for this Quick Start.

```shell
mkdir frankenstein
cd frankenstein
yo thing
```

You will only be asked to provide a name of your website. It should be short but expressive: e.g. "The Kitchen", "Brazil Travels". Use "Frankenstein" to follow this Quick Start exactly, but it's not compulsory.

- Now wait...

- Files are created.

- The finished message appears.

### What `yo thing` makes

Assuming you ran **generator-thing** in a folder call `app_name`:

```asciiart
|- .gitignore
|- init_app_name.sh
|- LICENSE.sh
|- manage.py
|- pyproject.toml
|- README.md
|- setup.py
|- app_name
    |- __init__.py
    |- models.py
    |- settings.py
    |- urls.py
    |- views.py
    |- wsgi.py
    |- static
        |- css
            |- app_name.min.css
            |- app_name.min.css.map
    |- templates
        |- app_name
            |- base.html
            |- home_page.html
            |- thing_delete.html
            |- thing_engaged.html
            |- thing_form.html
            |- thing_listed.html
            |- thing_many.html
            |- thing_optimize.html
            |- thing_page.html
|- schemaorg
    |- data
        |- releases
            |- 3.9
                |- all-layers.jsonld
```

### Seeing is Believing

Now you can follow the onscreen instructions to setup a `virtualenv`; install **django-thing**; and run the website. Assuming you followed the Quick Start exactly (which will assume for the rest of this guide), you'll type the following.

```
source venv-frankenstein/bin/activate[.fish]
pip install -e .
./init_frankenstein.sh
```

Navigate to <http://localhost:8000>

You will now see a working app!

### What we learnt

- How to use `yo thing` to install a **django-thing** app.

## Exercise 2 `SITE_THING`

There are a handful of settings in **django-thing**, which are described in the [settings](/eliothing/django-thing/settings) documentation. For now you can use the defaults for all but `SITE_THING`.

Your entire website is define by the `SITE_THING` setting and it's the first thing you'll need to check. For a very basic application it could also be the last!

The `SITE_THING` setting describes the pages you want, which Schema models and fields you intend to use on each page. If you open your settings file you will find `yo thing` has already added the following simple example to get you started.

**example 1**

```
SITE_THING = {
  "page": {
    "Home": {
      "engage": {"WebPage": {"field": ["name", "description"]}},
      "list": {"Photograph": {"field": ["name", "image"]}}
    },
    "About": {
      "engage": {
        "AboutPage": {"field": ["name", "description"]},
        "Organization": {"field": ["name", "telephone", "email"]}
      }
    }
  }
}
```

Here is a quick breakdown of the core sections. The keywords should be read as verbs.

### `page`

All the dictionary keys inside a `page` section will become Pages on the website. In this example there are two pages, "Home" and "About". **django-thing** automatically builds a menu and adds it to the context of any page.

### `engage`

There are two "roles" for Schema Types for every Page. The `engage` Role is like a singleton: 1 record of each specified model in this section will be rendered, in order, at the top of the page. For instance, in **example 1**, on the "Home" page, there will be a "WebPage" model whose fields you edit, and are rendered to a template. By default in **django-thing** templates, only fields with data are rendered.

### `list`

The second kind of Role is for querysets of the Schema Types listed. These are rendered on the page below the `engage` section. You can have 1 or more Types and they will be listed in the order mention in `SITE_THING`.

By combining the `engage` and `list` sections, you can easily create diverse pages of content.

### `field`

Represents the names of Schema properties the Page should field. If this is not provided **django-thing** will display all the fields in the editing form. By default though **django-thing** displays any field which has data, regardless of what is being fielded.

For **example 1**, we can expect to see the properties for the "name" and "description" fields at the top of the webpage, and a list of "images" below. On the "About" page we will see a "name" and "description"; plus the "name", "telephone" and "email" of the site's LocalBusiness below that.

<https://schema.org> Types have lots of properties, so it's a good idea to list the specific fields you want to edit on forms. All fields are available though - and you can use the Browse section of the website to see Forms which show all of them.

Let's look at another more complex example:

**example 2**

```
SITE_THING = {
  "page": {
    "Home": {
      "engage": {"WebPage": {"field": ["name", "description"]}},
      "list": {
        "Photograph": {
          "paginate": 20,
          "template": "movies/_thumbs.html",
          "field": ["name", "image"]
        }
      }
    },
    "sep1": {"status": "spacer"},
    "Movies": {
      "engage": {"WebPage": {"field": ["name", "description"]}},
      "page": {
        "Reviews": {
          "list": {
            "Review": {
              "paginate": 5,
              "template": "movies/_reviews.html",
              "field": ["image", "name", "text", "reviewRating"]
            }
          }
        },
        "News": {
          "list": {
            "CreativeWork": {
              "paginate": 10,
              "field": ["image", "name", "text", "creator"],
            }
          }
        }
      }
    }
  }
}
```

In this example, we've introduced some sub Pages - breaking the "Movies" section of the website into two Pages.

We're specified a "template" for the "Review" and "Photograph" type. We've also indicated that we want to paginate the "Review" and "News" lists.

For now though, let's change the `SITE_THING` generator-thing created for you.

- Change your `SITE_THING` to the following:

```
SITE_THING = {
  "page": {
    "Home": {
      "engage": {"WebPage": {"field": ["name", "description"]}},
      "list": {
        "Photograph": {
          "paginate": 10,                            # <-- add this
          "template": "frankenstein/_thumbs.html",   # <-- add this
          "field": ["name", "image"]
        }
      }
    },
    "About": {
      "template": "frankenstein/about_page.html",
      "engage": {
        "AboutPage": {"field": ["name", "description"]},
        "Organization": {"field": ["name", "telephone", "email"]},
      }
    }
  }
}
```

<header><strong>NB</strong>: There are two templates defined now which we will create in the next Exercise. Until you do, the page will respond with a Django error message.</header>

### What we learnt

The `SITE_THING` setting is ... everything. The entire website hangs off it. The site's menu is automatically generated against it. The Django models are dynamically migrated and built from it. If you use all the default **django-thing** templates, you don't need to do much more than edit `SITE_THING`. New pages and features can easily be added to the site by simply adding a new section to this dictionary.

In this section we learnt how `SITE_THING` works to:

- Create pages.
- Select different <https://schema.org> Types to drive the data for your website.
- Add separator in site menus.

We also looked at:

- Some of `SITE_THING`'s settings.
- Adjusting the properties of a Schema Type in the `SITE_THING`

Hopefully you've got a sense of how and why the `SITE_THING` is so important. There is more information in the specification page in this documentation. See [SITE_THING](/eliothing/django-thing/site_thing)

## Exercise 3 Custom Templates

### Page Templates

In the previous Exercise we specified a custom template for the "About" page called `frankenstein/about_page.html`.

- Create a new template called `frankenstein/templates/frankenstein/about_page.html`:

```
{% extends 'frankenstein/thing_page.html' %}

{% block precontent %}
  <p>Thank you for your interest in Frankenstein. You can also find us on Social Media at the following links.</p>
  <ul>
    <li><a href="twitter.com/frankenstein">Twitter</a></li>
    <li><a href="facebook.com/frankenstein">Facebook</a></li>
  </ul>
{% endblock precontent %}
```

This extends the default template for all pages (which **generator-thing** created for you).

We've added a new `block` tag called "precontent". This will need to be defined on the `base.html` file.

- Open `frankenstein/templates/frankenstein/base.html` and add this `block` tag above the `block content`

```
{% block precontent %}{% endblock precontent %}  <! -- add this -->
{% block content %}{% endblock content %}
```

This is all standard Django stuff - the only difference is that **django-thing** knows to collect the `frankenstein/about_page.html` defined in the `SITE_THING` rather than the one defined in your `FrankensteinPageView` in `frankenstein/views.py`.

- Refresh the "About" page to see your custom content.

### Detail Template

In **Exercise 2** you also defined a different template for the "Photograph" Schema Type in its `list` Role. It's time to create that file. This is not a Page template, but will be used to render the PhotoGraph queryset on the "Home" page.

The default template for rendering an instance of a Schema Type in a list is `thing/_thing_listed.html`, which is described on the [templates](/eliothing/django-thing/templates) specification in this documentation. We will use it as a starting point for our new `frankenstein/_thumbs.html` template. I like to use an underscore to indicate it is not a page level template. This is not compulsory.

- Create the file in your app, saving the file (in the normal Django way) to the folder `frankenstein/templates/frankenstein/_thumbs.html`. Add the following content, which is this is default template for a Page with a `list` role. I copied it from [templates](/eliothing/django-thing/templates).

```
<ul>
  {% for thing in object_list %}
    <li>
      <a href="{% url 'engage.thing' page_name thing_name thing.pk %}">{{ thing.name }} »</a>
    </li>
    {% empty %}
    <li>No
      {{ thing_name }}.</li>
  {% endfor %}
</ul>
{% if is_paginated %}
  <p>
    {% with is_paginated=is_paginated %}
    {% with paginator=paginator %}
    {% with page_obj=object_list %}
    {% include 'thing/_thing_paginator.html' %}
    {% endwith %}
    {% endwith %}
    {% endwith %}
  </p>
{% endif %}
```

Let's change this now so that it can display thumbnails instead.

<header><strong>NB</strong>: For now, we'll use inline styles for this... but you'll probably want to marry this process with your CSS throughput. I recommend looking at the <a href="/eliothing/genes">genes</a> app and using <a href="/eliosin">eliosin</a> for <em>patterns</em> that will help you do this expediency.</header>

For now, we'll use inline styles for this...

- Change the template to this:

```
{% load thing_tags %}
<style>
  body>section:after {
    clear: both;
    content: '';
    display: block;
    height: 0;
    width: 0;
  }
  section>figure {
    float: left;
    margin: 6px;
    max-height: 100px;
    max-width: 100px;
    padding: 6px;
  }
  section>figure>img {
    margin: 0;
  }
</style>
<section>
{% for thing in object_list %}
  <figure>
    <a href="{% url 'engage.thing' page_name thing_name thing.pk %}">
      {% if thing.image %}
      <img
        src="{% thumbnail thing.image '100x100' %}"
        alt="{{ thing.name }}" />
      {% else %}
        <a href="{% url 'update.thing' page_name 'list' thing_name thing.pk %}">Upload</a>
      {% endif %}
    </a>
  </figure>
{% endfor %}
</section>
{% if is_paginated %}
  <p>
    {% with is_paginated=is_paginated %}
    {% with paginator=paginator %}
    {% with page_obj=object_list %}
    {% include 'thing/_thing_paginator.html' %}
    {% endwith %}
    {% endwith %}
    {% endwith %}
  </p>
{% endif %}
```

There is just a little bit to unpack here.

We've created a very simple style for floating thumbnails in a `section` tag.

We're using **easy_thumbnails** to generate a thumbnail, which has been installed for you by **django-thing**. **django-thing** also comes with prepared settings for **easy_thumbnails**, plus a template_tag called `thumbnail` which we loaded from `thing_tags`. One of the settings allows for a "100x100" sized thumbnail, but there are others you can use as detailed in the settings documentation.

- [Settings](/eliothing/django-thing/settings)
- [Template Tags](/eliothing/django-thing/template_tags)

If there is no image uploaded, we provide a link direct to the edit form for the Photograph.

Otherwise, the template is similar to the default. We have the same "for loop" iterating the `object_list` - which will be a queryset of the Photograph model. We maintain the same URL which the user can use to engage with the photo on its own page and see the image in full (also edit or delete it there). We left the paginator as it was.

### What we learnt

- How to create a special template for a specific Schema Type in a specific Role on a specific Page.
- That THING comes with **easy_thumbnails** and some ready made template_tags.

## Next Steps

That completes this quick start. Now all that is left is to start building your website from **django-thing**.

- Customise the main templates.
- Create special templates for the layout

  - Templates for engage roles with different Schema Types.
  - Templates for list roles with different Schema Types.
  - Templates for forms.
