# django-thing Urls

Create a `my_app/urls.py` file in your app with the following patterns.

See the relevant section in the [Views Documentation](/eliothing/django-thing/views) for an explanation of these Urls.

```
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from myapp.views import (
    MyAppCreateView,
    MyAppUpdateView,
    MyAppDeleteView,
    MyAppEngageView,
    MyAppListView,
    MyAppManyView,
    MyAppOptimizeView,
    MyAppPageView,
    MyAppHomeView,
)

urlpatterns = [
    path(
        "cr/<str:page_slug>/<str:role>/<str:thing>",
        MyAppCreateView.as_view(),
        name="create.thing",
    ),
    path(
        "u/<str:page_slug>/<str:role>/<str:thing>/<int:pk>",
        MyAppUpdateView.as_view(),
        name="update.thing",
    ),
    path(
        "d/<str:page_slug>/<str:thing>/<int:pk>",
        MyAppDeleteView.as_view(),
        name="delete.thing",
    ),
    path(
        "e/<str:page_slug>/<str:thing>/<int:pk>",
        MyAppEngageView.as_view(),
        name="engage.thing",
    ),
    path(
        "l/<str:page_slug>/<str:thing>",
        MyAppListView.as_view(),
        name="list.things",
    ),
    path(
        "m/<str:page_slug>/<str:thing>/<int:pk>/<str:field>",
        MyAppManyView.as_view(),
        name="many.things",
    ),
    path(
        "o/<str:page_slug>/<str:thing>",
        MyAppOptimizeView.as_view(),
        name="optimize.things",
    ),
    path("p/<str:page_slug>", MyAppPageView.as_view(), name="engage.page"),
    path("", MyAppHomeView.as_view(), name="home"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
