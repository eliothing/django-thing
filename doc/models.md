# django-thing Models

All Django's models are built dynamically by **django-thing**. You'll need a `models.py` file with the following code.

```
from django.conf import settings
from thing.thing import ThingOfDjangoModel
from thing.models import every_model


every_model(
    ThingOfDjangoModel(
        settings.SCHEMA_PATH, settings.SITE_THING, settings.THING_DEPTH
    ).build()
)
```

That's all you need. Now you can run `makemigrations` and `migrate` as normal.

## System fields

All **django-thing** models have some special "system" fields. These are hidden from the user on the front end.

### `elio_key`

Because you might have the same Schema Types on different pages, **django-thing** uses the `elio_key` field to ensure that the correct instance/queryset is shown on the right pages. `elio_key` should therefore be unique to the context you want the model's data to render. `elio_key` is basically the Url of the `page` (from `SITE_THING`) you created it on + the Model name.

### `elio_page`

Is the page it was created on. We use this for the `get_absolute_url` property of the model - it's the redirect to Url when you delete or update the instance of the model.

### `elio_role`

The `elio_role` is either "engage" or "list" depending on the Model's "role" on a `page`. See the `SITE_THING` documentation in the [Quickstart](/eliothing/django-thing/quickstart.html) for more information.
