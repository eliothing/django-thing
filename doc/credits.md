# django-thing Credits

## Core, Thanks

- [Schema.org](https://schema.org)
- [django-polymorphic](https://github.com/django-polymorphic/django-polymorphic)
- [ckeditor-4](https://ckeditor.com/ckeditor-4/)

## These were useful

- [stackoverflow:Viktor_Danyliuk:dynamic-models-django](https://stackoverflow.com/questions/46213379/dynamic-models-django)
- [django-livereload-server>](https://github.com/tjwalch/django-livereload-server)
- [djangosnippets:widgets](https://djangosnippets.org/snippets/2027/)

## These might be useful

- [summernote](https://summernote.org)
- [castable-model-mixin](https://sophilabs.co/blog/castable-model-mixin)
- [foreign-keys-addedit-button](https://djangopy.org/how-to/reverse-engineering-of-the-django-admin-foreign-keys-addedit-button/)

## Artwork

- [pixabay:Colin Behrens](https://pixabay.com/illustrations/thing-dns-biology-2331995/)
- [wikimedia:Evolution](https://commons.wikimedia.org/wiki/File:Evolution_WEB%3D.jpg)
- [wikimedia:Chromosome_5](https://commons.wikimedia.org/wiki/File:Chromosome_5_%285887837880%29.jpg)
- [Mahmoud Ahmed](https://pixabay.com/photos/thing-3d-biology-genetic-research-5297378/)
- [Monomethylhydrazine](https://commons.wikimedia.org/wiki/File:Monomethylhydrazine_molecule_ball.png)
