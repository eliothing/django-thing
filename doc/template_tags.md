# django-thing Template Tags

## `thumbnail`

Create a thumbnail from an image using **easy_thumbnails** and providing a thumbnail size preset in **django-thing** [settings](/eliothing/django-thing/settings)

```
@register.simple_tag(name="thumbnail")
def thumbnail(image_path, dimensions):
    thumb = watermark(image_path, dimensions)
    return thumb
```

### Usage

```
{% load thing_tags %}

<img
  src="{% thumbnail object.image '100x100' %}"
  alt="{{ object.name }}" />
```

Available sizes:

- 100x100
- 300x300
- 640x480
- 960x720

## `has_no_merit`

Evaluate a field value and determine whether it is worth rendering to the page. We use this to prevent **django-thing** from printing every field of an object - even if empty - but only fields with information that is useful.

```
@register.filter("has_no_merit")
def has_no_merit(value):
    return (
        not value
        or value == "False"
        or value is None
        or value == "None"
        or str(value) == "0"
        or str(value) == "0.0"
        or boolean(value.strip())
    )
```

### Usage

```
{% for field_name, field_type, field in object.get_fields %}
  {% if not field|has_no_merit %}
    {{ field }}
  {% else %}
    {# nothing to see here #}
  {% endif %}
{% endfor %}
```

## `is_in`

A useful utility to determine whether a value exists in a comma separated list. We use it to exclude rendering certain fields of a model if they match a preset list (i.e. system fields).

```
@register.filter("is_in")
def is_in(value, comma_list):
    return value in comma_list.split(",")
```

### Usage

```
{% if value|is_in:"dog,cat,mouse,parrot,rat" %}
  You're a pet!
{% endif %}
```

- See [templates](templates.html#``)

## `flip_boo`

Utility template_tag used in flip a Boolean value from True to False and vice versa. We use it to flip the "menu" variable in the `_menu.html` template which allows us to switch from `menu` to `nav` when we build the nested menu. You can't do this: `{% with boolean_val=not boolean_val %}`, so instead we do this `{% with boolean_val=boolean_val|flip_boo %}`

### Usage

```
{% with boolean_val=boolean_val|flip_boo %}
<{{ boolean_val|yesno:"menu,nav" }}>
  <label>Sub Menu</label>
  {% with boolean_val=boolean_val|flip_boo %}
  <{{ boolean_val|yesno:"menu,nav" }}>
    <a href="/subitem">subitem</a>
  </{{ boolean_val|yesno:"menu,nav" }}>
  {% endwith %}
</{{ boolean_val|yesno:"menu,nav" }}>
{% endwith %}
```

## `dirt`

Utility template_tag used in development to render to a website the `dir` of any template variable, i.e. dish the dirt on what kind of object it is.

```
@register.filter("dirt")
def dirt(value):
    return dir(value)
```

### Usage

```
{{ value|dirt }}
```

## `qrystr_in_concert`

Utility to handle multiple querystrings (in concert). We use it so that we can run multiple paginators on a page. The template tag takes a querystring key and value and updates it; then returns the whole queryset back.

```
# need to add 'django.core.context_processors.request' to settings.TEMPLATES
@register.simple_tag(takes_context=True)
def qrystr_in_concert(context, name, value):
    request = context["request"]
    qs = request.GET.copy()
    qs[name] = value
    return qs.urlencode()
```

### Usage

In the following example, `qrystr_in_concert` will maintain all three querystrings for filtering the "currentpage".

```
<b>Filters</b>
{% for col in col_list %}
<a href="{% url 'page' currentpage %}?{% qrystr_in_concert 'color' col %}">Filter color {{ col }}</a>
{% endfor %}
{% for cat in cat_list %}
<a href="{% url 'page' currentpage %}?{% qrystr_in_concert 'category' cat %}">Filter category {{ cat }}</a>
{% endfor %}
{% for size in size_list %}
  <a href="{% url 'page' currentpage %}?{% qrystr_in_concert 'size' size %}">Filter size {{ size }}</a>
{% endfor %}
```
