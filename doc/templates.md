# django-thing Templates

**django-thing** doesn't come with any page level templates. It only has a small number of templates designed to render Schema based Django model instances and querysets; or **django-thing** View context variables. They fully work, but you'll probably want to replace them with your own to meet the design requirements for you site. However they are worth referencing as the starting point.

## Default Templates for Content Detail

### `_menu.html`

```
{% load thing_tags %}
{# start an outer tag block #}
{% with menutag=menu|yesno:"menu,nav" %}
<{{menutag}}>
  {# for every item paged at this level #}
  {% spaceless %}
  {% for page_key, page_vals in page_menu.page.items %}

    {# check for a submenu and put the next item in a separate menu block #}
    {% if page_vals.page %}
      <{{menutag}}>
      {% endif %}

      {# print the item as a label or a link #}
      {% if page_vals.status == "label" %}
        <label>{{ page_key }}</label>
      {% else %}
        <a
          href="{{ page_vals.url }}"
          {% if page_vals.status %}
          class="{{ page_vals.status }}"
          {% endif %}>[{{ page_key|default:app_name }}]</a>
      {% endif %}

      {# check for a submenu and call this template recursively #}

      {% if page_vals.page %}
        {# make the submenu the outer menu of the next recursion  #}
        {% with page_menu=page_vals %}
        {# switching from menu2nav, tag2nav, etc #}
        {% with menu=menu|flip_boo %}
        {% include 'thing/_menu.html' %}
        {% endwith %}
        {% endwith %}

        {# close the submenu block #}
      </{{menutag}}>
    {% endif %}

  {% endfor %}
  {% endspaceless %}
</{{menutag}}>
{% endwith %}
```

You'll notice that `_menu.html` calls itself recursively and so is able to render a nested array of sub menus. In this vein we follow the pattern in **eliosin**'s [innocent](/eliosin/innocent) stylesheet of nesting menu/nav alternatively to build deep menus. _eliosin doesn't like lots of nested div or ul tags with class names_

#### Usage

```
{% with menu_level=page_menu %}
{% with menu=False %}
{% include 'thing/_menu.html' %}
{% endwith %}
{% endwith %}
```

- `menu` is a tag name.

  - If `True`, `_menu.html` writes a `menu` tag as the parent, then nests `nav` tags for sub menus.
  - If `False`, `_menu.html` writes a `nav` tag as the parent, then nests `menu` tags for sub menus.

- `page_menu` comes out of the template context for `EveryView`, and is the top level of the menu.

### `_page_engaged.html`

```
{% for engaged in page_engages %}

  {% with thing_name=engaged.thing_name %}
  {% with object=engaged.engage %}
  {% with many=engaged.many %}
  {% include engaged.template %}
  {% endwith %}
  {% endwith %}
  {% endwith %}

  <p>
    <a href="{% url 'update.thing' page_slug 'engage' engaged.thing_name engaged.engage.pk %}">
      <button>Update
        {{ engaged.thing_name }}</button>
    </a>
  </p>
{% endfor %}
```

This will render the `page_engages` variable which is in template context for `EveryPageView` (i.e. Django's `TemplateView`).

#### Usage

```
{% include 'thing/_page_engaged.html' %}
```

### `_page_listed_paginator.html`

The standard `_thing_paginator.html` template expects standard Django template variables (`paginator` and `page_obj`) from a Django `ListView`. This is a way to add multiple paginators from within a `EveryPageViewMixin` with a `page_lists` variable. `paginate_type` is appended to each `page` querystring key in the paginator so that each paginator works in concert with the others.

```
<p>
  {% with paginator=listed.paginator %}
  {% with page_obj=listed.list %}
  {% with paginate_type=listed.paginate_type %}
  {% include 'thing/_thing_paginator.html' %}
  {% endwith %}
  {% endwith %}
  {% endwith %}
</p>
```

#### Usage

```
<p>
  {% include 'thing/_page_listed_paginator.html' %}
</p>
```

### `_page_listed.html`

```
{% for listed in page_lists %}
  {% if listed.title %}
    <h2>{{ listed.title }}</h2>
  {% endif %}

  {% with object_list=listed.list %}
  {% with thing_name=listed.thing_name %}
  {% include listed.template %}
  {% endwith %}
  {% endwith %}

  {% if listed.is_paginated %}
  <p>
    {% include 'thing/_page_listed_paginator.html' %}
  </p>
  {% endif %}
  <p>
    <a href="{% url 'create.thing' page_slug 'list' listed.thing_name %}">
      <button>Add
        {{ listed.thing_name }}</button>
    </a>
  </p>
{% endfor %}
```

This will render the `page_lists` variable which is in template context for `EveryPageView` (i.e. Django's `TemplateView`).

#### Usage

```
{% include 'thing/_page_listed.html' %}
```

### `_thing_confirm_delete.html`

```
<form method="post">
  {% csrf_token %}
  <p>Are you sure you want to delete "{{ object }}"?</p>
  <button type="submit">Confirm</button>
  <a href="{{ object.get_absolute_url }}">
    <button type="button">Cancel</button>
  </a>
</form>
```

This is pretty standard Django and will delete the Schema `object` variable which is in template context for `EveryDeleteView` (i.e. Django's `DeleteView`).

#### Usage

```
{% include 'thing/_thing_confirm_delete.html' %}
```

### `_thing_engaged.html`

```
{% load thing_tags %}

<main
  itemscope="itemscope"
  itemtype="http://schema.org/{{ thing_name }}">

  {% for field_name, url, height, width in object.get_images %}
    {% if url == "None" %}
      {# nothing to see here #}
    {% elif url %}
      <figure>
        <image
          itemprop="{{ field_name }}"
          src="{{ url }}"
          height="{{ height }}"
          width="{{ width }}"/>
        <figurecaption>{{ object.name }}</figurecaption>
      </figure>
    {% endif %}
  {% endfor %}

  {% if object.name %}
    <h1 itemprop="name">{{ object.name }}</h1>
  {% endif %}

  {% if object.alternateName %}
    <h2 itemprop="name">{{ object.alternateName }}</h2>
  {% endif %}

  <dl>
    {% for field_name, field_type, field in object.get_fields %}
      {% if field|has_no_merit %}
        {# nothing to see here #}
      {% elif field_name|is_in:"name,alternateName,polymorphic_ctype,id,elio_key,elio_page,elio_role" %}
        {# handled differently #}
      {% elif field_type == "OneToOneField" %}
        {# nothing to see here #}
      {% elif field %}
        <dt>{{ field_name }}</dt>
        <dd itemprop="{{ field_name }}">
          {% if field_type == "URLField" %}
            <a href="{{ field }}">{{ field }}</a>
          {% elif field_type == "TextField" %}
            {{ field|safe }}
          {% elif field_type == "ForeignKey" %}
            <link itemprop="availability" href="http://schema.org/{{ field.thing_name }}"/>
            <a href="{{ field.get_absolute_url }}">{{ field }}</a>
          {% elif field_name == "telephone" %}
            <a href="tel:{{ field }}">{{ field }}</a>
          {% elif field_type == "EmailField" %}
            <a href="mailto:{{ field }}">{{ field }}</a>
          {% else %}
            {{ field }}
          {% endif %}
        </dd>
      {% endif %}
    {% endfor %}

  </dl>
  {% if many %}
    {% include "thing/_thing_many.html" %}
  {% endif %}

</main>
```

This will render the `object` variable which is in template context for `EveryEngageView` (i.e. Django's `DetailView`) or from the `thing/_page_engaged` for a collection of object instances in `page_engages` of `EveryPageView` (i.e. Django's `TemplateView`). .

#### Usage

```
{% include 'thing/_thing_engaged.html' %}
<p>
  <a href="{% url 'update.thing' page_name thing_name object.pk %}">
    <button type="button">Update</button>
  </a>
  <a href="{% url 'delete.thing' page_name thing_name object.pk %}">
    <button type="button">Delete</button>
  </a>
  <a href="{{ object.get_absolute_url }}">
    <button type="button">Back</button>
  </a>
</p>
```

### `_thing_field.html`

```
{% if field.field.widget.input_type == 'checkbox' %}
  {{ field }}
{% endif %}
{% if field.label %}
  <label for='{{ field.id_for_label }}'>
    {{ field.label }}
    {% if field.help_text %}
      <span>{{ field.help_text|safe }}</span>{% endif %}
  </label>
{% endif %}
{% if not field.field.widget.input_type == 'checkbox' %}
  {{ field }}
{% endif %}
{% for error in field.errors %}
  <span class="errorlist">
    <mark>{{ error }}</mark>
  </span>
{% endfor %}
```

#### Usage

See `_thing_form.html` below.

### `_thing_form.html`

```
{% if not no_token %}
  {% csrf_token %}
{% endif %}
{% if form.non_field_errors %}
  {{ form.non_field_errors }}
{% endif %}
<fieldset>
  {% if legend %}
    <legend>{{ legend }}</legend>
  {% endif %}

  {% for field in form.visible_fields %}
    {% include 'thing/_thing_field.html' %}
  {% endfor %}

  {% for formset_form in formset %}
    <legend>
      {{ formset_legend }}
      {% if formset_add %}
        {{ forloop.counter|add:formset_add }}:
      {% else %}
        {{ forloop.counter }}:
      {% endif %}
    </legend>
    {% for field in formset_form.visible_fields %}
      {% include 'thing/_thing_field.html' %}
    {% endfor %}
    {% for hidden in formset_form.hidden_fields %}
      {{ hidden }}
    {% endfor %}
  {% endfor %}
</fieldset>
{% for hidden in form.hidden_fields %}
  {{ hidden }}
{% endfor %}
```

This will render the `object` variable which is in template context for `EveryUpdateView` (i.e. Django's `UpdateView`) or `EveryCreateView` (i.e. Django's `CreateView`).

#### Usage

```
<form enctype="multipart/form-data" method="post">
  {% include 'thing/_thing_form.html' %}
  <p>
    <button type="submit">Update</button>
  </p>
  {% if object.pk %}
    <a href="{{ object.get_absolute_url }}">
      <button type="button">Cancel</button>
    </a>
  {% endif %}
</form>
```

### `_thing_listed.html`

```
<ul>
  {% for thing in object_list %}
    <li itemscope="itemscope" itemtype="http://schema.org/{{ thing_name }}">
      <a
        href="{% url 'engage.thing' page_slug thing_name thing.pk %}"
        itemprop="name">{{ thing.name }}»</a>
    </li>
    {% empty %}
    <li>No
      {{ thing_name }}.</li>
  {% endfor %}
</ul>
{% if is_paginated %}
<p>
  {% include 'thing/_thing_paginator.html' %}
</p>
{% endif %}
```

This will render the `object_list` variable which is in template context for `EveryListView` (i.e. Django's `ListView`) or from the `thing/_thing_listed` for a collection of querysets in `page_lists` of `EveryPageView` (i.e. Django's `TemplateView`).

#### Usage

```
{% include 'thing/_thing_listed.html' %}
<p>
  <a href="{% url 'create.thing' 'list' page_slug thing_name %}">
    <button type="button">Add
      {{ page_name }}</button>
  </a>
</p>
```

### `_thing_many.html`

```
{% load thing_tags %}
<dl>
  {% for prop_name, many_things in many.items %}
    <dt itemprop="{{ prop_name }}">
      {{ prop_name }}
      <a
        href="{% url 'many.things' page_slug thing_name object.pk prop_name %}">
        [+]
      </a>
    </dt>
    {% for many in many_things %}
      <dd
        itemscope="itemscope"
        itemtype="http://schema.org/{{ many.listed_thing }}">
        <a
          href="{% url 'engage.thing' page_slug many.listed_thing many.many_listed.pk %}"
          itemprop="name">{{ many.many_listed.name }}»</a>
      </dd>
    {% endfor %}
  {% endfor %}
</dl>
```

### `_thing_paginator.html`

```
{% load thing_tags %}

{% if page_obj.has_previous %}
  <a href="?{% qrystr_in_concert 'page'|add:paginate_type page_obj.previous_page_number %}">
    <button>«</button>
  </a>
{% else %}
  <button disabled="disabled">«</button>
{% endif %}
{% for pg in paginator.page_range %}
  <a href="?{% qrystr_in_concert 'page'|add:paginate_type pg %}">
    <button {% if page_obj.number == pg %}class="selected"{% endif %}>
      {{ pg }}
    </button>
  </a>
{% endfor %}

{% if page_obj.has_next %}
  <a href="?{% qrystr_in_concert 'page'|add:paginate_type page_obj.next_page_number %}">
    <button>»</button>
  </a>
{% else %}
  <button disabled="disabled">»</button>
{% endif %}
```

#### Usage

See `_thing_listed.html` and `_page_listed.html` for examples of how to call this template.

## genes

The **eliothing** [genes](/eliothing/genes) app has many more examples you can use named after Schema Things. We recommend you add and use these to get you started more rapidly.

## Templates for your App.

Here is a list of page level templates you will need in your app with the bare minimum of tags. **generator-thing** creates these for you if you call `yo thing` in your project folder.

### `base.html`

```
{% load static %}
<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      {% block title %}{% endblock title %}
      {{ app_name }}
    </title>
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat:400,800&display=swap"
      rel="stylesheet">
  </head>
  <body>
    {% block nav %}{% endblock nav %}
    {% block h1 %}{% endblock h1 %}
    {% block content %}{% endblock content %}
  </body>
</html>
```

### `home_page.html`

```
{% extends 'app_name/base.html' %}
{% load static %}

{% block title %}
  App Name
{% endblock title %}

{% block nav %}{% endblock nav %}

{% block h1 %}{% endblock h1 %}

{% block content %}
  <!-- login in here -->
  <p>
    <a href="{% url 'engage.page' home_page|slugify %}">
      <button><big>engage</big></button>
    </a>
  </p>
{% endblock content %}
```

### `thing_delete.html`

```
{% extends 'app_name/base.html' %}
{% load static %}
{% block title %}
  Delete
  {{ thing_name }}
{% endblock title %}

{% block h1 %}
  Delete
  {{ thing_name }}?
{% endblock h1 %}

{% block content %}
  {% spaceless %}
    {% include 'thing/_thing_confirm_delete.html' %}
  {% endspaceless %}
{% endblock content %}
```

### `thing_engaged.html`

```
{% extends 'app_name/base.html' %}
{% load static %}
{% block title %}
  {{ thing_name }}
{% endblock title %}

{% block content %}
  {% spaceless %}
    {% include 'thing/_thing_engaged.html' %}
  {% endspaceless %}
  <p>
    <a href="{% url 'update.thing' page_slug 'list' thing_name object.pk %}">
      [Update]
    </a><a href="{% url 'delete.thing' page_slug thing_name object.pk %}">
      [Delete]
    </a><a href="{{ object.get_host_page }}">
      [Back]
    </a>
  </p>
{% endblock content %}
```

### `thing_form.html`

```
{% extends 'app_name/base.html' %}
{% load static %}

{% block title %}
  {{ thing_name }}
{% endblock title %}

{% block h1 %}
  {{ thing_name }}
{% endblock h1 %}

{% block content %}
  <form enctype="multipart/form-data" method="post">
    {% include 'thing/_thing_form.html' %}
    <p>
      <button type="submit">Update {{ thing_name }}</button>
    </p>
    {% if object.pk %}
      <a href="{{ object.get_absolute_url }}">
        <button type="button">Cancel</button>
      </a>
    {% endif %}
  </form>
{% endblock content %}
```

### `thing_listed.html`

```
{% extends 'app_name/base.html' %}
{% load static %}
{% block title %}
  {{ thing_name }}
{% endblock title %}

{% block h1 %}
  {{ thing_name }}
{% endblock h1 %}

{% block content %}
  {% spaceless %}
    {% include 'thing/_thing_listed.html' %}
  {% endspaceless %}
  <p>
    <a href="{% url 'create.thing' page_slug 'list' thing_name %}">[Add
      {{ thing_name }}]</a>
  </p>
{% endblock content %}
```

### `app_name/thing_many.html`

```
{% extends 'app_name/thing_form.html' %}
{% load static %}

{% block title %}
  {{ many_thing }}
  {{ block.super }}
{% endblock title %}

{% block h1 %}
  {{ thing_name }} {{ prop_name }}/{{ many_thing }}
{% endblock h1 %}
```

### `thing_optimize.html`

```
{% extends 'app_name/base.html' %}
{% load static %}

{% block title %}
  Optimize
{% endblock title %}

{% block h1 %}
  Optimize
{% endblock h1 %}

{% block content %}
  <p>
    Stats coming
  </p>
{% endblock content %}
```

### `thing_page.html`

```
{% extends 'app_name/base.html' %}
{% load static %}

{% block title %}
  {{ page_name }}
{% endblock title %}

{% block h1 %}
  {{ page_name }}
{% endblock h1 %}

{% block content %}
  {% include 'thing/_page_engaged.html' %}
  {% include 'thing/_page_listed.html' %}
{% endblock content %}
```
