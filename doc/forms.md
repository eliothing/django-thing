# django-thing ModelForms

You won't need a forms.py file in your app. **django-thing** comes with a method called `every_form` which will dynamically build a Django `ModelForm` for every Model you selected from Schema Class Types. **django-thing**'s View Mixins call `every_form` as required, but if you create custom Views in your app to do things slightly differently, while still using Schema, it will help to understand the optional parameters for `every_form`.

You could even customise your own method.

## `every_form`

... looks like this:

```
def every_form(any_model, elio_key, page_link, field_thing=[]):
    class EveryForm(ModelForm):
        def __init__(self, *args, **kw):
            super().__init__(*args, **kw)
            for f, v in self.fields.items():
                if v.__class__.__name__ == "DateField":
                    self.fields[f].widget = AnyDateInput()
                elif v.__class__.__name__ == "TimeField":
                    self.fields[f].widget = AnyTimeInput()
                elif v.__class__.__name__ == "DateTimeField":
                    self.fields[f].widget = AnyDateTimeInput()
                elif v.__class__.__name__ == "EmailField":
                    self.fields[f].widget = AnyEmailInput()
                elif v.__class__.__name__ == "ModelChoiceField":
                    # self.fields[f].widget = AnyForeignKeyInput()
                    # print(
                    #     getattr(self.instance.__class__, f).field.related_model
                    # )
                    pass
                elif v.__class__.__name__ in [
                    "DurationField",
                    "FloatField",
                    "IntegerField",
                ]:
                    self.fields[f].widget = AnyNumberInput()
                elif v.__class__.__name__ in [
                    "CharField",
                    "URLField",
                    "BooleanField",
                    "ImageField",
                    "TypedChoiceField",
                ]:
                    pass
                else:
                    print("Needs widget?", v.__class__.__name__)
            self.fields["elio_key"].widget = HiddenInput()
            self.fields["elio_key"].initial = elio_key
            self.fields["elio_page"].widget = HiddenInput()
            self.fields["elio_page"].initial = page_link

        field_order = [
            "name",
            "alternateName",
            "disambiguatingDescription",
            "image",
            "description",
            "sameAs",
        ]

        class Meta:
            model = any_model
            exclude = ["pk", "polymorphic_ctype", "elio_role"]
            if field_thing:
                fields = field_thing + ["elio_key", "elio_page"]

    return EveryForm
```

### Parameters

#### `any_model`

A Django model type. All **django-thing** Views have a self.Model property representing the Django model in URL context. If you want to hard code a model from schema you can use the `apps` module from Django to do this yourself:

```
from django.apps import apps
DjangoModelForEveryForm = apps.get_model(
          model_name='Organization', app_label=my_app_name
      )
form = every_form(DjangoModelForEveryForm)
```

#### `elio_key`

All **django-thing** models have some special "system" fields. Because you might have the same Schema Types on different pages, **django-thing** uses the `elio_key` field to ensure that the correct instance/queryset is shown on the right pages. `elio_key` should therefore be unique to the context you want the model's data to render. See [models](/eliothing/django-thing/models) for more information.

### `page_link`

The Url of the page calling `every_form`. This contains the important Url args `page_name` and `thing_name`.

### `field_thing`

A list of fields (and probably a sub list of all the fields mention in Schema). The form will only render for these fields, unless this list is empty: Then it will render the full set of fields.
