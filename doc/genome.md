# django-thing genome

**django-thing** comes with a very useful Schema explorer called `genome`. This management command guides finding Schema Types to drive your site's data, and help you select the best fields for your Page's Schema Roles.

You can use it to list information about Types or Properties in the Schema. You could also visit the <https://schema.org> for the same purpose.

## Type `genome`

This lists:

1. The description from <https://schema.org>
2. Which classes it subclasses. This is important because **django-thing** uses Class Inhertitance to build Django models.
3. The properties of this class - not including the ones it will inherit.

Examples

```
django-admin genome Thing
django-admin genome Place
django-admin genome LocalBusiness
```

You can also add the optional parameter `-d`. This will display a list of any Types which are dependant on it. This can be really useful because Microdata is all about semantics. For instance if you are creating an app to gather content about the government services offered in your town, don't just use the "Organization" type... instead type:

```
django-admin genome Organization -d
```

You will see that GovernmentOrganization, LibrarySystem, EducationalOrganization, TouristInformationCenter, GovernmentOffice, Library, EmploymentAgency, EmergencyService, RecyclingCenter are all Types which are inherited from Organization. If I were creating such an app, I would choose all these Types and either have them on the same page (using a filter on that page), or split the site into pages for each category of service.

## Property `genome`

`genome` can tell if you are querying a Type or property by default, so the command is the same. The only difference is that the `-d` is not relevant.

The `genome` of a property lists:

1. The description from <https://schema.org>
2. All the Types which have this property as a field.
3. All the Classes of the property - where Classes is just another way of saying Type.

While we are in the subject of Property types/classes, it is helpful to understand how **django-thing** treats these.

**django-thing** tames Schema in many ways, but Property types is an area most in need of taming!

In building Django models, **django-thing** takes the following Types as Primitive. They will be automatically resolved to a standard Django `models.Field`.

- <http://schema.org/Boolean>
- <http://schema.org/DateTime>
- <http://schema.org/Date>
- <http://schema.org/Number>
- <http://schema.org/Text>
- <http://schema.org/Time>
- <http://schema.org/Duration>
- <http://schema.org/Float>
- <http://schema.org/Integer>
- <http://schema.org/Quantity>
- <http://schema.org/URL>

All other Property Types are Schema Types and can be resolved to Django `models.ForeignKey`. **django-thing** will decide automatically whether to use a Primitive or Schema Type depending on the `THING_DEPTH` setting explained in [settings](/eliothing/django-thing/settings).
