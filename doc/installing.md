# Installing django-thing

- [django-thing Prerequisites](/eliothing/django-thing/prerequisites.html)

# Installing with generator-thing

We provide a Yeoman generator to get you started. **generator-thing** scaffolds a new **Django** app with **django-thing** installed - all the required settings sketched out - and all the basic views, urls, and templates you need to get started are also generated.

- [generator-thing Documentation](/eliothing/generator-thing)
- [Installing generator-thing](/eliothing/generator-thing/installing.html)

# Installing with pip

Alongside an existing **Django** project you can also use **generator-thing** - but if you prefer to manage things manually, you can pip install **django-thing**.

```
pip install elio-thing
```

Now you need to add a **Django** app with all the files described in the documentation.

- [Settings](/eliothing/django-thing/settings)
- [Models](/eliothing/django-thing/models)
- [Views](/eliothing/django-thing/views)
- [Urls](/eliothing/django-thing/urls)
- [Templates](/eliothing/django-thing/templates)

Are you sure you wouldn't prefer to just type `yo thing`?

**Run the tests:**

```shell
find . -name '*.pyc' -delete
find . -name '__pycache__' -delete
py.test -x
```

## Publish

Activate the virtualenv.

```
python3 setup.py sdist bdist_wheel
twine upload dist/*
# Enter YOUR-USERNAME and YOUR-PASSWORD
```

Testing it:

```
pip install elio-thing
```

**Test Publish**

```
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
# Enter YOUR-USERNAME and YOUR-PASSWORD
cd some/test/folder
virtualenv --python=python3 venv-thing_django
source venv-thing_django/bin/activate
# or
source venv-thing_django/bin/activate.fish
python3 -m pip install --index-url https://test.pypi.org/simple/ thing
```
