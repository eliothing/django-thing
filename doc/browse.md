# django-thing Browse

Included by default in all **django-thing** menus is a "Browse Things" section. This lists all the Schema Types which form a backbone of your website. Some of those Types will be included in your `SITE_THING`, but some will be there as dependencies.

## What is "Browse Things"?

The "Browse Things" section can be used to see a queryset of all the data in your site, regardless of its Role. You can use this area for the following purposes:

### Content overview

"Browse Things" gives you an overview of all your data, regardless of which page it is on.

### Edit all the fields

When you edit content from one of the site's Pages, the form is limited to the fields listed in the `field` section of the `SITE_THING`. However when **django-thing** dynamically builds **Django** Models, it add all the properties of every Type.

By selecting an instance to edit in the "Browse Things" section the form will render with all the properties. This can be useful for adding data to a record outside the scope in `SITE_THING`. By default **django-thing** returns all the fields in View context, but will only render to the template fields which are not null or empty.

### Add/edit related data

Some of the properties of your Schema Types in `SITE_THING` will be ForeignKeys, related to other Schema Types. Those Types may not be listed in your Pages, so "Browse Things" gives you a chance to add related records for Types outside of the `SITE_THING`. Those things will then show up in the drop down of any Form with a ForeignKey type.

## Editing in "Browse Things"

It should be noted that any records you **create** from "Browse Things" will never appear in either the `engage` or `list` area of a Page. If you want a record to appear on a Page, create it from that Page.

However, records **edited** in "Browse Things" will stick to their Page.

_TODO: Maybe change this so that records edited from Browse show the elio_key and elio_page fields for manually moving them to a different page?_

## Access

By default **django-thing** comes with no authenticated access. Obviously one of the first things you'll want to do is create a superuser and hide all the editing features. We suggest you don't show the "Browse Things" section to web visitors because otherwise search engines will spider those pages and it will dilute the SEO of the site if the information is foind in more than one place. One of the reasons for using **django-thing** is that we can publish data with Microdata already in place.
