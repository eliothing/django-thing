# SITE_THING

This page describes the current and proposed specification for the 'SITE_THING' setting.

## Overview

The structure should look like this:

```
SITE_THING = {
  'page': {
    'Page1': {
      'engage': {
        'SchemaType1': { ... settings for SchemaType1 ... },
        'SchemaType2': { ... }
      },
      'list': {
        'SchemaType1': { ... },
        'SchemaType4': { ... },
        'SchemaType3': { ... }
      },
      'page': {
        .. repeat this pattern for sub pages ...
      }
    },
    'Page2':  { ... etc ... },
  }
}
```

## `page`

The website's main pages are created by first defining a `page` key as the first and only direct key of `SITE_THING`.

Inside `page` add a dictionary for each Page you want where the key is the name of the Page as it will appear in the menu and URL. (See below).

- The nesting goes `page` > "PageName(s)" > "role(s)" > "SchemaType(s)"
- Sub pages are structured: `page` > "PageName(s)" > `page` > "PageName(s)" > `page` > "PageName(s)"

## Page Specification

Pages are defined as dictionaries and can have the following keys:

### `url`

If you need custom links in the menu, add a `url` property. In the following `SITE_THING`, while **django-thing** will create the link for "Page1" and build Schema content, it will render "More Nuts", "More Blogs" and "Logout" as given.

```
'page': {
  'Page1': {
    'engage': {
      'SchemaType1': { ... settings for SchemaType1 ... },
    }
  },
  'More Nuts': { 'url': 'http://www.peanuts.com/' },
  'More Blogs': {
      "status": "label",
      "page": {
          "Blog1": {"url": "http://one.com"},
          "Blog2": {"url": "http://two.com"},
  },
  'Logout': { 'url': '/' },
}
```

### `template`

By default **django-thing** will use the template created by **generator-thing** and described in the [templates specification](/eliothing/django-thing/templates) of this documentation. Optionally you can create a special template for each page.

In the following example we are defining a website which uses no Schema Types. Effectively this would work perfectly well to create a static site.

```
'page': {
  'Intro': { 'template': 'frankenstein/intro_page.html' },
  'Hobbies': { 'template': 'frankenstein/hobbies_page.html' },
  'Family': { 'template': 'frankenstein/family_page.html' },
  'Pets': { 'template': 'frankenstein/pets_page.html' },
  'Garden': { 'template': 'frankenstein/garden_page.html' },
  'Monster': { 'template': 'frankenstein/monster_page.html' },
  'Igor': { 'template': 'frankenstein/igor_page.html' }
}
```

### `engage` and `list`

These two keys are (what we call) the page Roles. Both are optional. See below.

### `page`

Create sub pages which appear as a sub menu for the Page. These links will also be available in the Page context so that your user can link to the Sub Pages from the Host Page.

`page` is iterative, so the pattern for the `SITE_THING` main `page` can be repeated as deeply as you dare.

## Schema Types

As mentioned above, Schema Types can appear as content on a page in 1 of 2 "Roles".

### `engage`

`engage` renders (to the page template) **1 instance** each of the Schema Types defined in the given order.

- `template` is optional but recommended. Default is `thing/_thing_engaged.html`
- `field` is optional but recommended. Limits the fields rendered in the edit form for this SchemaType on this Page in this Role. All the fields for the Type are in the database, but limiting the fields on the form helps focus the User on the important ones.

```
'engage': {
  'Thing': {
    'template': 'frankenstein/_page_intro.html',
    'field': ['name', 'description'],
  },
  'GeoCoordinates': {
    'template': 'frankenstein/_google_map.html',
    'field': ['address', 'longitude', 'longitude']
  }
},
```

- `many` is optional. It also takes a list of fields from this Schema Types, but rather than handling them as single fields, **django-thing** uses these to provide a Many2Many relationship between the "engaged" Thing and Things relating to the SchemaType of the field (if available - defaults to Thing). `many` offers the facility to do something fundamental in microdata. For instance a "TrainingCourse" Schema might have `many` "events".

```
'engage': {
  'TrainingCourse': {
    'template': 'frankenstein/_page_intro.html',
    'field': ['name', 'description'],
    'many': ['event']
  },
},
```

In the HTML you might see this as:

```
<div itemtype="http://schema.org/TrainingCourse" itemscope="itemscope">
  <p itemprom="name">Learning elio-thing **the elioWay**</p>
  <p itemprom="description">Learn how to build websites **the elioWay**!</p>
  <ul itemprom="event">
    <li itemscope="itemscope" itemtype="http://schema.org/TrainingEvent">
      <span itemprom="location">London</span>
      <span itemprom="startDate">4 Sep 2020</span>
    </li>
    <li itemscope="itemscope" itemtype="http://schema.org/TrainingEvent">
      <span itemprom="location">New York</span>
      <span itemprom="startDate">11 Oct 2020</span>
    </li>
    <li itemscope="itemscope" itemtype="http://schema.org/TrainingEvent">
      <span itemprom="location">Mumbai</span>
      <span itemprom="startDate">24 Nov 2020</span>
    </li>
  </ul>
</div>
```

### `list`

`list` renders (to the page template) **1 queryset** each of the Schema Types defined in the given order.

Listed SchemaTypes have the following optional properties

- `field` The same rules apply as with `engage`.
- `many` The same rules apply as with `engage`.
- `template` Default is `thing/_thing_listed.html`.
- `paginate` will place a paginator for each Schema under the list. Multiple paginators will work in concert.

```
'list': {
  'TouristDestination': {
    'template': 'frankenstein/_places_to_visit.html',
    'paginate': 12,
    'field': ['name', 'description', 'touristType', 'address', 'longitude', 'longitude']
  },
  'FoodEstablishment': {
    'template': 'frankenstein/_restaurents.html',
    'paginate': 6,
    'field': ['name', 'description', 'menu', 'address', 'longitude', 'longitude']
    'many': ['reviews']
  }
}
```
