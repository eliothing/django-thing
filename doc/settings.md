# django-thing Settings

There are some ready-made settings you will need to add from the `thing` app. Just add this at the top of your settings file:

```
from thing.settings import *
```

_Or copy and paste them across from the [settings.py](https://gitlab.com/eliothing/django-thing/blob/master/django-thing/settings.py) file._

As well as some useful settings for **easy_thumbnails**...

```
THUMBNAIL_PROCESSORS = easy_thumbnails_defaults.THUMBNAIL_PROCESSORS
THUMBNAIL_DEFAULT_OPTIONS = {"size": (100, 100), "crop": "center"}
THUMBNAIL_ALIASES = {
    "": {
        "960x720": {
            "size": (960, 720),
            "crop": False,
            "autocrop": True,
            "background": "white",
            "upscale": True,
        },
        "640x480": {
            "size": (640, 480),
            "crop": False,
            "autocrop": True,
            "background": "white",
            "upscale": True,
        },
        "300x300": {
            "size": (300, 300),
            "crop": "center",
            "autocrop": True,
            "background": "white",
            "upscale": True,
        },
        "100x100": THUMBNAIL_DEFAULT_OPTIONS,
    }
}
```

... there are these 4 important settings.

## THING presets

`THING_SHORT_TEXT_FIELDS` and `THING_LONG_TEXT_FIELDS` are two lists of Schema property names which are hardcoded for **Django** `model.Field` types.

`THING_SHORT_TEXT_FIELDS` are all set to a **Django** `models.CharField` type with the required character lengths, e.g.:

```
THING_SHORT_TEXT_FIELDS = (
  "name": 512,
  "disambiguatingDescription": 512,
  ...
)
```

`THING_LONG_TEXT_FIELDS` is simply a list of Schema properties which we want to be `models.TextField` types. IN **django-thing** these are rendered as HTMLEditors. You can of course override both these settings adding or removing property names. Remember, they must be the names of properties from <https://schema.org> Types.

## Schema version

**django-thing** comes with a version of <https://schema.org> taken from their [repo](https://github.com/schemaorg/schemaorg). You can change the following settings to different versions, or even from other Schema's which support `jsonld` format. To use a different version, clone their repo into your project and change the `SCHEMA_VERSION`, `SCHEMA_PATH` `THING_BASE_MODEL_NAME` and `THING_ENUMERATION_MODEL_NAME` settings.

```
SCHEMA_VERSION = "3.9"
SCHEMA_PATH = f"schemaorg/data/releases/{SCHEMA_VERSION}/all-layers.jsonld"
THING_BASE_MODEL_NAME = "Thing"
THING_ENUMERATION_MODEL_NAME = "Enumeration"
```

## `thing` app

Add `thing` to your `INSTALLED_APPS` list. I've also added `easy_thumbnails` to mine. Don't forget to add your own app!

```
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contentTypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "easy_thumbnails",
    "thing",
    "my_app",
]
```

## urls

**django-thing** doesn't provide any urls, so you'll have to create your own. **generator-thing** will add these for you. If your app is the primary project, don't forget to change the ROOT_URLCONF setting. If not, include the Urls file in your `ROOT_URLCONF` file.

```
ROOT_URLCONF = "my_app.urls"
```

## `APP_NAME`

Add an APP_NAME setting. We need this for dynamically creatings Django models.

```
APP_NAME = "my_app"
```

## `THING_DEPTH`

Add the `THING_DEPTH` settings. This is best set to `0` unless you need a highly complex database.

**The way it works is this.**

In your `SITE_THING` you selected a small number of Schema Types for various pages of your site (see [Quickstart](quickstart.html)). As well as these Types, **django-thing** also adds any Schema Types which your selected models require for dependencies. For instance, in **example 1** in the quickstart, the "LocalBusiness" Type inherits from the "Organization" type - so your app must (and **django-thing** will) dynamically create both the "LocalBusiness" and the "Organization" Django models. We can call these your "Core Types".

Here's where it gets complicated.

- All those Types have properties and some of those properties are also Types.
- And those Types have properties which might also be Types.
- And those Types have properties which might also be Types.
- And those Types have properties which might also be Types.

Before you know it, **django-thing** is creating 100s of Models and ForeignKeys, all interweaving and deeply nested.

By limiting the `THING_DEPTH`, we force **django-thing** to swop those Types for primitive Types (i.e. standard Django model.Fields) instead of going too deep. <https://schema.org> generally has both Primitive and Class Types alternatives for every property - if there is no Primitive type mentioned, **django-thing** will substitute the Class Type text for a general Text property when the `THING_DEPTH` has been reached.

NB: **django-thing** will still use a Type for any property but only if it's already been collected.

```
THING_DEPTH = 0
```

A depth of 0 or 1 is generally advised. Any deeper than 3 and you're basically modelling the entire <https://schema.org> set of Class Types. **django-thing** is an experimental project and we have no idea how Django would cope with such a large number of deeply inherited models with as crisscross of ForiegnKeys. Contributors wanted to help start solving and investigating such issues.

## What we learnt

The following settings:

- `THING_SHORT_TEXT_FIELDS` and `THING_LONG_TEXT_FIELDS`
- `SCHEMA_VERSION` and `SCHEMA_PATH`
- `INSTALLED_APPS`
- `ROOT_URLCONF`
- `APP_NAME`
- `THING_DEPTH`
