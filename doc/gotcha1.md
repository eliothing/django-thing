# django-thing Gotcha 1

In an earlier version, the DJANGO_FIELDS dictionary in `thing.py` was global to the module. Problem was... when a model field type was accessed more than once this creates a issue during migrate and migration.

For instance:

```shell
chromosomes.Thing.sameAs: (models.E006) The field 'sameAs' clashes with the field 'sameAs' from model 'chromosomes.thing'.
chromosomes.Thing.sameAs: (models.E006) The field 'sameAs' clashes with the field 'sameAs' from model 'chromosomes.thing'.
chromosomes.Thing.sameAs: (models.E006) The field 'sameAs' clashes with the field 'sameAs' from model 'chromosomes.thing'.
chromosomes.Thing.sameAs: (models.E006) The field 'sameAs' clashes with the field 'sameAs' from model 'chromosomes.thing'.
```

The explanation for these errors is that 4 fields were using the same _instance_ of `models.URLField` type.

Moving the DJANGO*FIELDS inside the `django_type_of` method fixed this problem so that each field type is a different \_instance* ... and the whole thing only took about 8 hours to work out.
