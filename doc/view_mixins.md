# django-thing View Mixins

**django-thing** views are driven by a handful of crucial Mixins. These are listed here describing what attributes the view provides.

See: `thing.view_mixins`

## `EveryThingMixin`

### `site_thing` property

This property deepcopies the `SITE_THING` setting described in the [Quickstart](/eliothing/django-thing/quickstart.html).

### `thing` property

This property returns an instance of the `ThingOfDjangoModel` class, which the other methods access to build a picture of your website.

### `page_name` property

This convenient property returns the name of the page from the URL args which will correspond to a `page` key in the `SITE_THING`. e.g. `FilmReviews`, `Suppliers`, `Bookings`, `AboutSite`.

### `thing_name` property

This convenient property returns the name of the current Schema Type from the URL args. e.g. `Thing`, `Place`, `WebPage`, `Hospital`

### `page_link` property

This convenient property returns a URL to the current page. There are two types of contexts: a URL which is a `page` in `SITE_THING`; and a `list` page for one of the Schema Types in your site backbone.

### `page_menu` property

This property builds a menu object we pass to the `_menu.html` template, which in turn renders an HMTL menu of the site which is responsive to the current page context. For instance it will display sub pages section of the menu when the URL is for the parent page or any of the other subpages. It will also return which menu item is currently active. The `page_menu` is also used to build the `breadcrumbs`.

### `Model` property.

This property returns the Model class of the current context. You can treat this property like any Django model, calling the `objects` property to query the table in the database. It determines the Model class from the `thing_name` property.

### `get_context_data` method

This sets the universe context for all pages in a **django-thing** website, sending the following values to any template:

- `app_name` your **django-thing** app name in `title()` format.
- `thing_name` property as described above.
- `page_name` property as described above.
- `home_page` conveniently returns the name/key of the sites first `page` in the `SITE_THING`.
- `page_menu` a dict representing the entire menu of the site which changes depending on the current URL.

## `EveryQuerySetMixin`

### `paginate_by` field

Defaults to 20\. You can change the pagination number for every Thing listed in your `SITE_THING`.

### `get_queryset` method

Returns the Django queryset for the Model of the `thing_name` in the URL, ordered by the `name` property.

## `EveryGetObjectMixin`

### `get_object` method

Returns the Django instance for the Model of the `thing_name` and `pk` in the URL.

## `EveryEditMixin`

### `get_form_class` method

Returns the Django standard form prepared for the Model of the `thing_name` in the URL. This is used in `CreateView` and `UpdateView`. The fields rendered by the template are controlled by the `SITE_THING` if accessing the Model from a `page`. Otherwise the form will display all the fields of the Model as specified in the Schema. For more information see the `every_form` method in the [forms][forms.html] section of the documentation.

## `EveryManyFormMixin`

FormView to create a new instance of ModelB which has a Many2Many relationship with an engaged instance of ModelA (artificially "hosting") related to one of ModelA's properties. `EveryManyFormMixin` overides `form_valid` to build a Many2Many relationship using the special `ElioMany` model.

### `parent` property

Returns the pk of the Parent Model from the URL. This Model will have 1 or more of its fields list with `many` roles in the `SITE_THING`.

### `prop_name` property

Returns the property name "hosting" the `many` relation from the URL.

### `many_thing` property

Returns the Schema Type of `prop_name`.

### `FieldModel` property

Returns a Model class we will use to create an new instance of the `many_thing`.

### `get_form_class` method

Returns a ModelForm class we will use to create an new instance of the `many_thing`.

### `form_valid` method

Overloads the `form_valid` of the `CreateView` and creates the Many2Many relationship between the `parent` and `many_thing`.

### `get_context_data` method

Puts the name of the many_thing and prop_name into template context.

## `ManyMixin`

Utility mixin to fetch an `ElioMany` queryset belonging to an engaged object one of its properties.

### `_many` method

Utility method to return a queryset of `ElioMany` objects filtered by `parent` and `many_thing` as well as Url context.

## `EveryEngageMixin`

For any object instance we engage, `EveryEngageMixin` puts 1 or more related querysets against any of its field names set up in a `many` role.

### `get_many` method

Marshalls the retrieval of a querysets of `ElioMany` objects filtered by `parent` and `many_thing`.

### `get_context_data` method

Puts the `many` querysets into context. `many` is a dict of keys which are property names each with a querysets relation the current object with many objects under the property name.

## `EveryDeleteMixin`

### `get_success_url` field

Return the `.page_link` property which will redirect to the `page` which previously hosted the deleted object.

## `EveryPageMixin`

### `get_page_content` method

When the URL relates to a `page` key in the `SITE_THING`, this builds the content of the page from the `engage` and `list` sections of the dictionary. This is called by the `get_context_data` method of this class.

### `get_context_data` method

Adds `page_engages` and `page_lists` to the context. These are dictionary objects which are rendered by **django-thing**'s and contain all the objects and querysets required to build the content for the given page. As well as the object instances and querysets, the two data sets also provide contextual information relevant to the content, like the paginator, the thing name, as well as any custom template which should be used to render the object or queryset (overriding **django-thing**'s default templates).
