# Development

.. is also a fine way to install **django-thing**, using GIT as a starting project.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliothing.git
cd eliothing
git clone https://gitlab.com/eliothing/django-thing.git
cd thing
virtualenv --python=python3 venv-django_thing
source venv-django_thing/bin/activate
pip install -r requirements/local.txt
```

## TODOS

1. TODOS
