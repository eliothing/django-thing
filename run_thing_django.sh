#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

export DJANGO_SETTINGS_MODULE="thing.settings"

touch thing.db # && rm thing.db
django-admin makemigrations thing
django-admin migrate --noinput --database=default
django-admin collectstatic --noinput
django-admin runserver 0.0.0.0:8000
