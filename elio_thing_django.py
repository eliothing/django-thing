import argparse

from thing.thing import thing

def run(args):
	arg1 = args.arg1
	arg2 = args.arg2
	arg3 = args.arg3
    thing(arg1, arg2, arg3)

def main():
	parser=argparse.ArgumentParser(description="thing app")
	parser.add_argument("-arg1", help="arg1", dest="arg1", type=str, required=True)
	parser.add_argument("-arg2", help="arg2", dest="arg2", type=str, required=True)
	parser.add_argument("-arg3", help="arg3", dest="arg3", type=str, required=False)
    parser.set_defaults(func=run)
	args=parser.parse_args()
	args.func(args)
